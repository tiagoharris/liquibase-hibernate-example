# liquibase-hibernate-example

A demo project to demonstrate how to integrate Liquibase with Spring Boot.

## Stack:
* [Liquibase](https://www.liquibase.org/)
* [Spring Boot](https://spring.io/projects/spring-boot)
* [Maven](https://maven.apache.org/)


