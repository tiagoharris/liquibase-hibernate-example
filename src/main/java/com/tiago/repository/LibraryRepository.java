package com.tiago.repository;

import org.springframework.data.repository.CrudRepository;

import com.tiago.entity.Library;

/**
 * Repository for {@link Library} entity.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
*/
public interface LibraryRepository extends CrudRepository<Library, Long> { }