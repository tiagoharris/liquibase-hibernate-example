package com.tiago.repository;

import org.springframework.data.repository.CrudRepository;

import com.tiago.entity.Book;

/**
 * Repository for {@link Book} entity.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
*/
public interface BookRepository extends CrudRepository<Book, Long> { }
